/**
 *
 */
package fr.ge.configuration.mock;

import fr.ge.configuration.mock.bean.GentUserBeanTest;
import fr.ge.configuration.mock.context.GentThreadContextTest;
import fr.ge.core.bean.UserBean;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.manager.IAccountManager;

/**
 * AccountManager is the integration class of the ct-authentification.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class AccountManagerMock implements IAccountManager {

    /**
     * {@inheritDoc}
     */
    @Override
    public UserBean getUserBean(final String idUser, final AccountUserBean accountUserBean) {
        // builds the UserFormsBean object which represents the user
        final UserBean userBean = new UserBean(idUser);
        return userBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void persistUserContext(final UserBean userBean) {
        // Persistence in the thread local
        GentThreadContextTest.setUser((GentUserBeanTest) userBean);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanUserContext() {
        // we empty the context
        GentThreadContextTest.unsetUser();
    }

}
