package fr.ge.configuration.mock.context;

import fr.ge.configuration.mock.bean.GentUserBeanTest;

/**
 * La Classe ThreadContext.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public final class GentThreadContextTest {

  /**
   * La constante thread local.
   */
  public static final ThreadLocal < GentUserBeanTest > LOCAL_USER = new ThreadLocal < GentUserBeanTest >();

  /**
   * Constructeur de la classe.
   *
   */
  private GentThreadContextTest() {
    super();
  }

  /**
   * Sets.
   *
   * @param userFormsBean
   *          l'utilisateur Gent
   */
  public static void setUser(final GentUserBeanTest userFormsBean) {
    LOCAL_USER.set(userFormsBean);
  }

  /**
   * Unset User.
   */
  public static void unsetUser() {
    LOCAL_USER.remove();
  }

  /**
   * Gets UserFormsBean .
   *
   * @return l'utilisateur UserFormsBean
   */
  public static GentUserBeanTest getUser() {
    return LOCAL_USER.get();
  }

}
