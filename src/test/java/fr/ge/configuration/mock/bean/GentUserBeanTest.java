/**
 * 
 */
package fr.ge.configuration.mock.bean;

import fr.ge.core.bean.UserBean;

/**
 * Bean Representant l'utilisateur pour l'application Gent Forms.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class GentUserBeanTest extends UserBean {

  /**
   * Civite de l'utilisateur : <br>
   * Monsieur <br>
   * Madame.
   */
  private String civilite;

  /**
   * Nom de l'utilisateur.
   */
  private String nom;

  /**
   * Prenom de l'utilisateur.
   */
  private String prenom;

  /**
   * Email de l'utilisateur.
   */
  private String email;

  /**
   * Langue de préférence de l'utilisateur : <br>
   * Exemple : fr.
   */
  private String langue;

  /**
   * Telephone de l'utilisateur au format E164 : <br>
   * Exemple : +33123456789.
   */
  private String telephone;

  /**
   * Constructeur de la classe.
   *
   * @param identifiant
   *          : identifiant de l'utilisateur
   */
  public GentUserBeanTest(final String identifiant) {
    super(identifiant);
  }

  /**
   * Accesseur sur l'attribut {@link #civilite}.
   *
   * @return String civilite
   */
  public String getCivilite() {
    return this.civilite;
  }

  /**
   * Mutateur sur l'attribut {@link #civilite}.
   *
   * @param civilite
   *          la nouvelle valeur de l'attribut civilite
   */
  public void setCivilite(final String civilite) {
    this.civilite = civilite;
  }

  /**
   * Accesseur sur l'attribut {@link #nom}.
   *
   * @return String nom
   */
  public String getNom() {
    return this.nom;
  }

  /**
   * Mutateur sur l'attribut {@link #nom}.
   *
   * @param nom
   *          la nouvelle valeur de l'attribut nom
   */
  public void setNom(final String nom) {
    this.nom = nom;
  }

  /**
   * Accesseur sur l'attribut {@link #prenom}.
   *
   * @return String prenom
   */
  public String getPrenom() {
    return this.prenom;
  }

  /**
   * Mutateur sur l'attribut {@link #prenom}.
   *
   * @param prenom
   *          la nouvelle valeur de l'attribut prenom
   */
  public void setPrenom(final String prenom) {
    this.prenom = prenom;
  }

  /**
   * Accesseur sur l'attribut {@link #email}.
   *
   * @return String email
   */
  public String getEmail() {
    return this.email;
  }

  /**
   * Mutateur sur l'attribut {@link #email}.
   *
   * @param email
   *          la nouvelle valeur de l'attribut email
   */
  public void setEmail(final String email) {
    this.email = email;
  }

  /**
   * Accesseur sur l'attribut {@link #langue}.
   *
   * @return String langue
   */
  public String getLangue() {
    return this.langue;
  }

  /**
   * Mutateur sur l'attribut {@link #langue}.
   *
   * @param langue
   *          la nouvelle valeur de l'attribut langue
   */
  public void setLangue(final String langue) {
    this.langue = langue;
  }

  /**
   * Accesseur sur l'attribut {@link #telephone}.
   *
   * @return String telephone
   */
  public String getTelephone() {
    return this.telephone;
  }

  /**
   * Mutateur sur l'attribut {@link #telephone}.
   *
   * @param telephone
   *          la nouvelle valeur de l'attribut telephone
   */
  public void setTelephone(final String telephone) {
    this.telephone = telephone;
  }

}
