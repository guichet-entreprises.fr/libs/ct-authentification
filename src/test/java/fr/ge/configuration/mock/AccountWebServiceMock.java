/**
 *
 */
package fr.ge.configuration.mock;

import java.util.Map;

import javax.jws.WebService;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.ge.core.exception.TechniqueRuntimeException;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.webservice.IAccountRestService;

/**
 * Mock de test des webservice.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
@Produces(MediaType.APPLICATION_JSON)
@WebService
public class AccountWebServiceMock implements IAccountRestService {

    /**
     * behavior id user -> HTTP Code Return.
     */
    private final Map<String, Integer> behavior;

    /**
     * Information always return when it's HTTP Code 200.
     */
    private final AccountUserBean mockUser;

    /**
     * Constructeur de la classe.
     *
     * @param behavior
     *            : behavior of the mock
     */
    public AccountWebServiceMock(final Map<String, Integer> behavior) {
        super();
        this.behavior = behavior;
        final AccountUserBean mockUser = new AccountUserBean();
        mockUser.setCivility("Monsieur");
        mockUser.setFirstName("prénom");
        mockUser.setLanguage("fr");
        mockUser.setEmail("test.unitaire@mock.fr");
        mockUser.setLastName("nom");
        mockUser.setPhone("+33836646464");
        mockUser.setPhoneCheck(true);
        this.mockUser = mockUser;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AccountUserBean registerUser(final AccountUserBean user) {
        if (this.behavior.containsKey(user.getTrackerId())) {
            throw new TechniqueRuntimeException("Problem.");
        } else {
            this.mockUser.setTrackerId(user.getTrackerId());
            return this.mockUser;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AccountUserBean readUser(final String id) {
        if (this.behavior.containsKey(id)) {
            this.mockUser.setTrackerId(id);
            return this.mockUser;
        } else {
            throw new TechniqueRuntimeException("Problem.");
        }
    }

}
