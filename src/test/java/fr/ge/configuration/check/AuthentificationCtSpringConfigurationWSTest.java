/**
 *
 */
package fr.ge.configuration.check;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

/**
 * Class which tests the Spring context loading.
 *
 * @author $Author: hhichri $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/applicationContext-ge-authentification-ct-test-config.xml",
  "classpath:/spring/applicationContext-ge-authentification-ct-ws-config.xml" })
@TestExecutionListeners(listeners = {DependencyInjectionTestExecutionListener.class })
public class AuthentificationCtSpringConfigurationWSTest {

  /**
   * Test.
   */
  @Test
  public void testSpringConfiguration() {

  }

}
