package fr.ge.ct.authentification.facade.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.exception.TechniqueRuntimeException;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.webservice.IAccountRestService;

/**
 * Tests {@link AccountFacadeImpl}.
 *
 * @author jpauchet
 *
 */
public class AccountFacadeImplTest {

    @Mock
    private IAccountRestService accountRestService;

    @InjectMocks
    private AccountFacadeImpl accountFacadeImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Tests {@link AccountFacadeImpl#readUser(String)}.
     *
     * @throws TechniqueException
     */
    @Test
    public void testRegisterUser() throws TechniqueException {
        // prepare
        final AccountUserBean responseUser = new AccountUserBean();
        responseUser.setFirstName("Jean");
        when(this.accountRestService.registerUser(any(AccountUserBean.class))).thenReturn(responseUser);

        // call
        final AccountUserBean resultOk = this.accountFacadeImpl.registerUser(new AccountUserBean());

        // verify
        verify(this.accountRestService).registerUser(any(AccountUserBean.class));
        assertThat(resultOk).isEqualTo(responseUser);
    }

    /**
     * Tests {@link AccountFacadeImpl#readUser(String)}.
     *
     * @throws TechniqueException
     */
    @Test
    public void testReadUser() throws TechniqueException {
        // prepare
        final AccountUserBean responseUser = new AccountUserBean();
        responseUser.setCivility("Monsieur");
        responseUser.setFirstName("Arnaud");
        responseUser.setLastName("BOIDARD");
        responseUser.setEmail("arnaud.boidard@yopmail.com");
        responseUser.setLanguage("fr");
        responseUser.setPhone("+33617243567");
        responseUser.setTrackerId("2016-11-VHB-LKD-55");
        responseUser.setPhoneCheck(true);

        when(this.accountRestService.readUser(eq("2018-07-AZE-RTY-42"))).thenReturn(responseUser);

        // call
        final AccountUserBean resultOk = this.accountFacadeImpl.readUser("2018-07-AZE-RTY-42");

        // verify
        verify(this.accountRestService).readUser(eq("2018-07-AZE-RTY-42"));
        assertThat(resultOk).isEqualTo(responseUser);
    }

    /**
     * Tests {@link AccountFacadeImpl#readUser(String)}.
     *
     * @throws TechniqueException
     */
    @Test(expected = TechniqueRuntimeException.class)
    public void testReadUserNotFound() throws TechniqueException {
        // prepare
        final AccountUserBean responseUser = new AccountUserBean();
        responseUser.setErrorMessage("an error message");
        responseUser.setErrorCode("an error code");
        final ResponseBuilder responseOk = Response.ok().entity(responseUser);
        Throwable userNotFound = new TechniqueRuntimeException("user not found");
        when(this.accountRestService.readUser(eq("2018-07-AZE-RTY-42"))).thenThrow(userNotFound);

        // call
        this.accountFacadeImpl.readUser("2018-07-AZE-RTY-42");
        // le résultat importe peu car une exception est levée
    }

}
