package fr.ge.ct.authentification.interceptor;

import static org.fest.assertions.Assertions.assertThat;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.ProcessingException;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.apache.cxf.transport.local.LocalConduit;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

import fr.ge.configuration.mock.AccountWebServiceMock;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.webservice.IAccountRestService;

/**
 * Classe de test for HTTPResponseInterceptor.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class HTTPResponseInterceptorTest {

    /**
     * End point adress.
     */
    private static final String ENDPOINT_ADDRESS = "local://authentification/";

    /**
     * Mock Behavior : idUser -> HTTP Code Response.
     */
    private static final Map<String, Integer> MOCK_BEHAVIOR = initMockBehavior();

    /**
     * Instantiate the Map :idUser -> HTTP Code Response.
     *
     * @return the Map initialize.
     */
    private static Map<String, Integer> initMockBehavior() {
        final Map<String, Integer> result = new HashMap<>();
        result.put("1", HttpURLConnection.HTTP_OK);
        // result.put("2", HttpURLConnection.HTTP_BAD_REQUEST);
        // result.put("3", HttpURLConnection.HTTP_NOT_FOUND);
        // result.put("4", HttpURLConnection.HTTP_BAD_METHOD);
        // result.put("5", 426);
        // result.put("6", HttpURLConnection.HTTP_INTERNAL_ERROR);
        // result.put("7", HttpURLConnection.HTTP_UNAVAILABLE);
        return Collections.unmodifiableMap(result);
    }

    /**
     * The server.
     */
    private static Server server;

    /**
     * Initialize the test class.
     */
    @BeforeClass
    public static void initialize() {
        startServer();
    }

    /**
     * Start the server.
     */
    private static void startServer() {
        final JAXRSServerFactoryBean sf = new JAXRSServerFactoryBean();
        sf.setResourceClasses(IAccountRestService.class);
        sf.setProvider(new JacksonJaxbJsonProvider());
        sf.setResourceProvider(IAccountRestService.class, new SingletonResourceProvider(new AccountWebServiceMock(MOCK_BEHAVIOR)));
        sf.setAddress(ENDPOINT_ADDRESS);
        server = sf.create();
    }

    /**
     * Stop and Destroy the server at the end.
     */
    @AfterClass
    public static void destroy() {
        server.stop();
        server.destroy();
    }

    /**
     * Methode de test getUserAccount.
     */
    @Test
    public void testCasPassant() {

        final List<Object> providers = new ArrayList<>();
        final JacksonJaxbJsonProvider jacksonJsonProvider = new JacksonJaxbJsonProvider();
        providers.add(jacksonJsonProvider);

        final IAccountRestService accountRestService = JAXRSClientFactory.create(ENDPOINT_ADDRESS, IAccountRestService.class, providers);
        WebClient.getConfig(accountRestService).getRequestContext().put(LocalConduit.DIRECT_DISPATCH, Boolean.TRUE);
        final HTTPResponseInterceptor interceptor = new HTTPResponseInterceptor();
        WebClient.getConfig(accountRestService).getInInterceptors().add(interceptor);

        // 1) cas nominal
        final AccountUserBean response200 = accountRestService.readUser("1");
        assertThat(response200.getTrackerId()).isEqualTo("1");

    }

    /**
     * Methode de test getUserAccount.
     */
    @Test
    public void testException() {

        final List<Object> providers = new ArrayList<>();
        final JacksonJaxbJsonProvider jacksonJsonProvider = new JacksonJaxbJsonProvider();
        providers.add(jacksonJsonProvider);

        final IAccountRestService accountRestService = JAXRSClientFactory.create(ENDPOINT_ADDRESS, IAccountRestService.class, providers);
        WebClient.getConfig(accountRestService).getRequestContext().put(LocalConduit.DIRECT_DISPATCH, Boolean.TRUE);
        final HTTPResponseInterceptor interceptor = new HTTPResponseInterceptor();
        WebClient.getConfig(accountRestService).getInInterceptors().add(interceptor);

        // 2) cas inconnu
        try {
            final AccountUserBean response400 = accountRestService.readUser("2");
        } catch (ProcessingException e) {
            assertThat(e.getCause().getMessage()).isEqualTo("Problem.");
        }

    }

}
