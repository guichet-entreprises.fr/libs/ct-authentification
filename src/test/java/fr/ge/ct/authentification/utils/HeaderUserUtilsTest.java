/**
 *
 */
package fr.ge.ct.authentification.utils;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.iterators.IteratorEnumeration;
import org.fest.assertions.Fail;
import org.junit.Test;

import fr.ge.core.exception.TechniqueRuntimeException;
import fr.ge.ct.authentification.bean.constante.IHeaderConstante;

/**
 * Test class of AccountUserUtils.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class HeaderUserUtilsTest {

  /**
   * Methode de test de l'attribut de la class.
   * 
   * @throws Exception
   *           : Exception
   */
  @Test
  public void classTest() throws Exception {
    final Constructor < ? > constructor = HeaderUserUtils.class.getDeclaredConstructor();
    assertThat(constructor.isAccessible()).isFalse();
    assertThat(Modifier.isPrivate(constructor.getModifiers())).isTrue();
    constructor.setAccessible(true);
    constructor.newInstance();
    assertThat(constructor.isAccessible()).isTrue();
    constructor.setAccessible(false);
  }

  /**
   * VerifyHeader test method.
   */
  @Test
  public void verifyHeaderTest() {

    final HttpServletRequest httpRequest = mock(HttpServletRequest.class);

    // TODO TO IMPLEMENT

    assertThat(HeaderUserUtils.verifyHeader(httpRequest)).isTrue();
  }

  /**
   * GetHeadersIdUser test method.
   */
  @Test
  public void getHeadersIdUserTest() {

    final HttpServletRequest httpRequest = mock(HttpServletRequest.class);

    // not passing test case : USER_GE_ID defined but null
    final Map < String, String > headersFalse = new HashMap < String, String >();
    headersFalse.put(IHeaderConstante.USER_GE_ID, null);
    final Enumeration enumerationHeadersFalse = new IteratorEnumeration(headersFalse.keySet().iterator());

    when(httpRequest.getHeader(IHeaderConstante.USER_GE_ID)).thenReturn(headersFalse.get(IHeaderConstante.USER_GE_ID));
    when(httpRequest.getHeaderNames()).thenReturn(enumerationHeadersFalse);

    String result = null;

    try {
      result = HeaderUserUtils.getHeadersIdUser(httpRequest);
      Fail.fail("TechniqueRuntimeException expected");
    } catch (final TechniqueRuntimeException e) {
      assertThat(result).isNull();
    }

    // passing test case : USER_GE_ID
    final Map < String, String > headersTrue = new HashMap < String, String >();
    headersTrue.put(IHeaderConstante.USER_GE_ID, "2016-11-VHB-LKD-55");
    final Enumeration enumerationHeadersTrue = new IteratorEnumeration(headersTrue.keySet().iterator());

    when(httpRequest.getHeader(IHeaderConstante.USER_GE_ID)).thenReturn(headersTrue.get(IHeaderConstante.USER_GE_ID));
    when(httpRequest.getHeaderNames()).thenReturn(enumerationHeadersTrue);

    assertThat(HeaderUserUtils.getHeadersIdUser(httpRequest)).isEqualTo("2016-11-VHB-LKD-55");

  }

  /**
   * GetHeadersIdUser test method.
   */
  @Test
  public void getHeadersAccountInfoTest() {

    final HttpServletRequest httpRequest = mock(HttpServletRequest.class);

    // not passing test case : USER_GE_ID defined but null
    final Map < String, String > headersFalse = new HashMap < String, String >();
    headersFalse.put(IHeaderConstante.USER_GE_ACCOUNT, null);
    final Enumeration enumerationHeadersFalse = new IteratorEnumeration(headersFalse.keySet().iterator());

    when(httpRequest.getHeader(IHeaderConstante.USER_GE_ACCOUNT)).thenReturn(headersFalse.get(IHeaderConstante.USER_GE_ID));
    when(httpRequest.getHeaderNames()).thenReturn(enumerationHeadersFalse);

    Map < String, String > result = null;

    try {
      result = HeaderUserUtils.getHeadersAccountInfo(httpRequest);
      Fail.fail("TechniqueRuntimeException expected");
    } catch (final TechniqueRuntimeException e) {
      assertThat(result).isNull();
    }

    // passing test case : USER_GE_ID
    final Map < String, String > headersTrue = new HashMap < String, String >();
    headersTrue.put(IHeaderConstante.USER_GE_ACCOUNT, "eyJvcmlnaW4iOiJGQyIsImNpdmlsaXR5IjoiTSIsIm5hbWUiOiJCT0lEQVJEIn0=");
    final Enumeration enumerationHeadersTrue = new IteratorEnumeration(headersTrue.keySet().iterator());

    when(httpRequest.getHeader(IHeaderConstante.USER_GE_ACCOUNT)).thenReturn(headersTrue.get(IHeaderConstante.USER_GE_ACCOUNT));
    when(httpRequest.getHeaderNames()).thenReturn(enumerationHeadersTrue);

    final Map < String, String > resultExpected = new HashMap < String, String >();
    resultExpected.put("origin", "FC");
    resultExpected.put("civility", "M");
    resultExpected.put("name", "BOIDARD");

    assertThat(HeaderUserUtils.getHeadersAccountInfo(httpRequest)).isEqualTo(resultExpected);
  }

}
