package fr.ge.ct.authentification.utils;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.mockito.Mockito;

/**
 * Test class of FilterUtils.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class FilterUtilsTest {

  /**
   * Methode de test de l'attribut de la class.
   * 
   * @throws Exception
   *           : Exception
   */
  @Test
  public void classTest() throws Exception {
    final Constructor < ? > constructor = FilterUtils.class.getDeclaredConstructor();
    assertThat(constructor.isAccessible()).isFalse();
    assertThat(Modifier.isPrivate(constructor.getModifiers())).isTrue();
    constructor.setAccessible(true);
    constructor.newInstance();
    assertThat(constructor.isAccessible()).isTrue();
    constructor.setAccessible(false);
  }

  /**
   * Methode de test ExcludeFromFilter.
   * 
   * @throws Exception
   *           : Exception
   */
  @Test
  public void testExcludeFromFilter() throws Exception {

    Pattern excludeUrlsPattern = Pattern.compile("status");

    // cas non passant
    HttpServletRequest httpRequest = Mockito.mock(HttpServletRequest.class);
    when(httpRequest.getRequestURI()).thenReturn("/togglz/index");
    assertThat(FilterUtils.excludeFromFilter(httpRequest, excludeUrlsPattern)).isFalse();

    // cas passant
    Mockito.reset(httpRequest);
    when(httpRequest.getRequestURI()).thenReturn("status");
    assertThat(FilterUtils.excludeFromFilter(httpRequest, excludeUrlsPattern)).isTrue();

    // cas non passant
    assertThat(FilterUtils.excludeFromFilter(httpRequest, null)).isFalse();
  }

}
