package fr.ge.ct.authentification.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

/**
 * The Class UidFactoryImplTest.
 */
public class UidFactoryImplTest {

    /**
     * Test format.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFormat() throws Exception {
        final UidFactoryImpl factory = new UidFactoryImpl();
        final String uid = factory.uid();

        Assert.assertNotNull(uid);

        final Matcher matcher = Pattern.compile("^([0-9]{4})-([0-9]{2})-([A-Z]{3})-([A-Z]{3})-([0-9]{2})").matcher(uid);

        Assert.assertNotNull(matcher);
        Assert.assertTrue(matcher.matches());
    }

}
