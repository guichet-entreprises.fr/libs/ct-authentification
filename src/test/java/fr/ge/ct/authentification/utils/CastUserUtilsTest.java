package fr.ge.ct.authentification.utils;

import static org.fest.assertions.Assertions.assertThat;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

import fr.ge.core.bean.GeUserBean;
import fr.ge.ct.authentification.bean.json.JsonUserBean;

/**
 * Test class of CastUserUtilTest.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class CastUserUtilsTest {

  /** Pattern date. **/
  private static final String DATE_CONNECTION_PATTERN = "yyyy-MM-dd hh:mm";

  /** Date format date. **/
  private static final DateFormat DATE_CONNECTION_FORMAT = new SimpleDateFormat(DATE_CONNECTION_PATTERN);

  /**
   * Methode de test de l'attribut de la class.
   * 
   * @throws Exception
   *           : Exception
   */
  @Test
  public void classTest() throws Exception {
    final Constructor < ? > constructor = CastUserUtils.class.getDeclaredConstructor();
    assertThat(constructor.isAccessible()).isFalse();
    assertThat(Modifier.isPrivate(constructor.getModifiers())).isTrue();
    constructor.setAccessible(true);
    constructor.newInstance();
    assertThat(constructor.isAccessible()).isTrue();
    constructor.setAccessible(false);
  }

  /**
   * Test Methode of castToGeUserBean.
   * 
   * @throws ParseException
   */
  @Test
  public void testCastToGeUserBean() throws ParseException {

    JsonUserBean jsonUser = new JsonUserBean();
    jsonUser.setId("GFT-FDT-6548");
    jsonUser.setCivility("Monsieur");
    jsonUser.setForName("Unitaire");
    jsonUser.setLanguage("fr");
    jsonUser.setMail("test.unitaire@mock.fr");
    jsonUser.setRescueMail("test.unitaire@mock.fr");
    jsonUser.setName("Test");
    jsonUser.setPhone("+33836646464");
    jsonUser.setPhoneCheck("true");
    final String connexionDate = "2017-05-04 11:34:17.123";
    jsonUser.setConnexionDate(connexionDate);

    GeUserBean geUserBean = CastUserUtils.castToGeUserBean(jsonUser);

    GeUserBean resultAttempt = new GeUserBean("GFT-FDT-6548");
    resultAttempt.setCivility("Monsieur");
    resultAttempt.setForName("Unitaire");
    resultAttempt.setLanguage("fr");
    resultAttempt.setMail("test.unitaire@mock.fr");
    resultAttempt.setName("Test");
    resultAttempt.setPhone("+33836646464");
    resultAttempt.setValidPhone(true);
    resultAttempt.setConnexionDate(DATE_CONNECTION_FORMAT.parse(connexionDate));

    assertThat(geUserBean).isEqualTo(geUserBean);
  }

  /**
   * Test Methode of JsonUserBean constructor.
   * 
   * @throws Exception
   *           :Exception
   */
  @Test
  public void testJsonCast() throws Exception {

    String jsonString = "{\"id\":\"2\",\"civility\":\"Monsieur\",\"name\":\"OLUBI\",\"forName\":\"Ademola\","
      + "\"language\":\"fr\",\"mail\":\"ademola.olubi@capgemini.com\",\"rescueMail\":\"ademola.olubi@capgemini.com\","
      + "\"phone\":\"33647943325\",\"phoneCheck\":\"false\",\"connexionDate\":\"2017-05-04 11:34:17.123\"}";
    JsonUserBean jsonResult = new JsonUserBean(jsonString);

    JsonUserBean resultAttempt = new JsonUserBean();
    resultAttempt.setId("2");
    resultAttempt.setCivility("Monsieur");
    resultAttempt.setForName("Ademola");
    resultAttempt.setLanguage("fr");
    resultAttempt.setMail("ademola.olubi@capgemini.com");
    resultAttempt.setRescueMail("ademola.olubi@capgemini.com");
    resultAttempt.setName("OLUBI");
    resultAttempt.setPhone("33647943325");
    resultAttempt.setPhoneCheck("false");
    resultAttempt.setConnexionDate("2017-05-04 11:34:17.123");

    assertThat(jsonResult).isEqualTo(resultAttempt);
  }
}
