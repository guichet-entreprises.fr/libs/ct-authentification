package fr.ge.ct.authentification.utils;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

/**
 * CookieUtils JUNIT tests.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class CookieUtilsTest {

    /**
     * Test getting a cookie from HTTP request
     */
    @Test
    public void testGetCookie() {
        // Cookie NASH present
        HttpServletRequest httpRequest = mock(HttpServletRequest.class);
        Cookie cookie = CookieUtils.createCookie(httpRequest, "test", "2017", 1, false, false);
        Cookie[] arrayCookies = { cookie };
        when(httpRequest.getCookies()).thenReturn(arrayCookies);
        Cookie resultCookie = CookieUtils.getCookie("test", httpRequest);
        assertThat(resultCookie).isNotNull();
    }

    /**
     * Test generate an uid.
     */
    @Test
    public void testUid() {
        assertThat(CookieUtils.uid()).isNotNull();
    }
}
