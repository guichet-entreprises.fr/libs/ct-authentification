/**
 *
 */
package fr.ge.ct.authentification.filter;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.iterators.IteratorEnumeration;
import org.fest.assertions.Fail;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import fr.ge.core.bean.UserBean;
import fr.ge.core.exception.TechniqueRuntimeException;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.bean.conf.AuthentificationConfigurationWebBean;
import fr.ge.ct.authentification.bean.constante.IHeaderConstante;
import fr.ge.ct.authentification.facade.IAccountFacade;
import fr.ge.ct.authentification.manager.IAccountManager;

/**
 * AccountSecurityFilter test class.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class AccountSecurityFilterTest {

    @InjectMocks
    private AccountSecurityFilter accountSecurityFilter;

    @Mock
    private IAccountManager<UserBean> accountManager;

    @Mock
    private HttpServletRequest httpRequest;

    @Mock
    private HttpServletResponse httpResponse;

    @Mock
    private FilterChain filterChain;

    @Mock
    private AuthentificationConfigurationWebBean authentificationConfigurationFilter;

    @Mock
    private IAccountFacade accountFacade;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void doFilterTestForAnonymous() throws IOException, ServletException {
        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);

        when(this.authentificationConfigurationFilter.getExcludeUrlsPattern()).thenReturn(Pattern.compile("^.*/((togglz|resources/|paiement/ipn|status\\?p=).*|status)$"));
        when(this.authentificationConfigurationFilter.getCookieAnonymousName()).thenReturn("nash");
        when(this.authentificationConfigurationFilter.isCookieAnonymousAllow()).thenReturn(true);
        when(this.authentificationConfigurationFilter.isCookieAnonymousHttpOnly()).thenReturn(false);
        when(this.authentificationConfigurationFilter.isCookieAnonymousSecure()).thenReturn(false);

        when(this.httpRequest.getRequestURI()).thenReturn("/cfe/login");

        final AccountUserBean accountUserBean = new AccountUserBean();
        accountUserBean.setCivility("Monsieur");
        accountUserBean.setFirstName("Arnaud");
        accountUserBean.setLastName("BOIDARD");
        accountUserBean.setEmail("arnaud.boidard@yopmail.com");
        accountUserBean.setLanguage("fr");
        accountUserBean.setPhone("+33617243567");
        accountUserBean.setTrackerId("2016-11-VHB-LKD-55");

        when(this.accountFacade.readUser(Mockito.anyString())).thenReturn(accountUserBean);

        // 1) passing test case
        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);

        ArgumentCaptor<Cookie> captor = ArgumentCaptor.forClass(Cookie.class);
        Mockito.verify(this.httpResponse, Mockito.times(1)).addCookie(captor.capture());

        List<Cookie> cookies = captor.getAllValues();
        assertEquals(1, cookies.size());
        assertEquals("nash", cookies.get(0).getName());
        assertNotNull(cookies.get(0).getValue());

        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);
        Mockito.verify(this.accountManager).getUserBean(Mockito.anyString(), Mockito.any());
        Mockito.verify(this.accountManager).cleanUserContext();

        // 1) passing test case
        Cookie[] arrayCookies = { cookies.get(0) };
        when(httpRequest.getCookies()).thenReturn(arrayCookies);
        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);
    }

    /**
     * DoFilter test method.
     *
     * @throws IOException
     *             : IOException
     * @throws ServletException
     *             : ServletException
     */
    @Test
    public void doFilterTest() throws IOException, ServletException {
        when(this.authentificationConfigurationFilter.getExcludeUrlsPattern()).thenReturn(Pattern.compile("^.*/((togglz|resources/|paiement/ipn|status\\?p=).*|status)$"));
        when(this.authentificationConfigurationFilter.isCookieAnonymousAllow()).thenReturn(false);

        // 0) not passing test case : no HTTP request
        final String result = null;
        try {
            this.accountSecurityFilter.doFilter(mock(ServletRequest.class), mock(ServletResponse.class), mock(FilterChain.class));
            Fail.fail("TechniqueRuntimeException expected");
        } catch (final TechniqueRuntimeException e) {
            assertThat(result).isNull();
            Mockito.verify(this.filterChain, Mockito.never()).doFilter(this.httpRequest, this.httpResponse);
        }

        // 1) not passing test case : USER_GE_ACCOUNT not defined
        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);
        final Map<String, String> headersFalse = new HashMap<String, String>();
        headersFalse.put(IHeaderConstante.USER_GE_ID, "2016-11-VHB-LKD-55");
        final Enumeration enumerationHeadersFalse = new IteratorEnumeration(headersFalse.keySet().iterator());

        when(this.httpRequest.getHeader(IHeaderConstante.USER_GE_ID)).thenReturn(headersFalse.get(IHeaderConstante.USER_GE_ID));
        when(this.httpRequest.getHeaderNames()).thenReturn(enumerationHeadersFalse);
        when(this.httpRequest.getRequestURI()).thenReturn("/cfe/login");

        final AccountUserBean accountUserBean = new AccountUserBean();
        accountUserBean.setCivility("Monsieur");
        accountUserBean.setFirstName("Arnaud");
        accountUserBean.setLastName("BOIDARD");
        accountUserBean.setEmail("arnaud.boidard@yopmail.com");
        accountUserBean.setLanguage("fr");
        accountUserBean.setPhone("+33617243567");
        accountUserBean.setTrackerId("2016-11-VHB-LKD-55");

        when(this.accountFacade.readUser("2016-11-VHB-LKD-55")).thenReturn(accountUserBean);

        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);

        Mockito.verify(this.accountManager).cleanUserContext();

        // 2) not passing test case : CSS status, paiement or togglz URL so not
        // filtered
        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);
        when(this.httpRequest.getRequestURI()).thenReturn("/resources/css/bootstrap.css");
        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);
        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);

        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);
        when(this.httpRequest.getRequestURI()).thenReturn("/status");
        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);
        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);

        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);
        when(this.httpRequest.getRequestURI()).thenReturn("/status?p=radiator");
        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);
        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);

        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);
        when(this.httpRequest.getRequestURI()).thenReturn("/paiement/ipn?numeroFormaliteCFE=G93010000359&idUtilisateur=12");
        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);
        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);

        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);
        when(this.httpRequest.getRequestURI()).thenReturn("/togglz/index");
        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);
        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);

        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);
        when(this.httpRequest.getRequestURI()).thenReturn("/togglz");
        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);
        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);

        // 3) passing test case
        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);
        final Map<String, String> headersTrue = new HashMap<String, String>();
        headersTrue.put(IHeaderConstante.USER_GE_ACCOUNT, "eyJvcmlnaW4iOiJGQyIsImNpdmlsaXR5IjoiTSIsIm5hbWUiOiJCT0lEQVJEIn0=");
        headersTrue.put(IHeaderConstante.USER_GE_ID, "2016-11-VHB-LKD-55");
        final Enumeration enumerationHeadersTrue = new IteratorEnumeration(headersTrue.keySet().iterator());

        when(this.httpRequest.getHeader(IHeaderConstante.USER_GE_ACCOUNT)).thenReturn(headersTrue.get(IHeaderConstante.USER_GE_ACCOUNT));
        when(this.httpRequest.getHeader(IHeaderConstante.USER_GE_ID)).thenReturn(headersTrue.get(IHeaderConstante.USER_GE_ID));
        when(this.httpRequest.getHeaderNames()).thenReturn(enumerationHeadersTrue);
        when(this.httpRequest.getRequestURI()).thenReturn("/cfe/login");

        // final AccountUserBean accountUserBean = new AccountUserBean();
        // accountUserBean.setCivility("Monsieur");
        // accountUserBean.setFirstName("Arnaud");
        // accountUserBean.setLastName("BOIDARD");
        // accountUserBean.setEmail("arnaud.boidard@yopmail.com");
        // accountUserBean.setLanguage("fr");
        // accountUserBean.setPhone("+33617243567");
        // accountUserBean.setTrackerId("2016-11-VHB-LKD-55");

        when(this.accountFacade.readUser("2016-11-VHB-LKD-55")).thenReturn(accountUserBean);

        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);

        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);
        Mockito.verify(this.accountManager).cleanUserContext();

        // 4) test case : the doFilter returns a technical exception
        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);
        doThrow(new TechniqueRuntimeException("Technical error")).when(this.filterChain).doFilter(this.httpRequest, this.httpResponse);

        final Map<String, String> headersException = new HashMap<String, String>();
        headersException.put(IHeaderConstante.USER_GE_ACCOUNT, "eyJvcmlnaW4iOiJGQyIsImNpdmlsaXR5IjoiTSIsIm5hbWUiOiJCT0lEQVJEIn0=");
        headersException.put(IHeaderConstante.USER_GE_ID, "2016-11-VHB-LKD-55");
        final Enumeration enumerationHeadersException = new IteratorEnumeration(headersTrue.keySet().iterator());

        when(this.httpRequest.getHeader(IHeaderConstante.USER_GE_ACCOUNT)).thenReturn(headersException.get(IHeaderConstante.USER_GE_ACCOUNT));
        when(this.httpRequest.getHeader(IHeaderConstante.USER_GE_ID)).thenReturn(headersException.get(IHeaderConstante.USER_GE_ID));
        when(this.httpRequest.getHeaderNames()).thenReturn(enumerationHeadersException);
        when(this.httpRequest.getRequestURI()).thenReturn("/cfe/login");

        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);

        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);
        Mockito.verify(this.accountManager).cleanUserContext();
    }

    @Test
    public void doFilterAnonyousSessionWithHeaders() throws IOException, ServletException {
        Mockito.reset(this.httpRequest, this.httpResponse, this.filterChain, this.accountManager);

        // -->Allow anonymous session
        when(this.authentificationConfigurationFilter.getExcludeUrlsPattern()).thenReturn(Pattern.compile("^.*/((togglz|resources/|paiement/ipn|status\\?p=).*|status)$"));
        when(this.authentificationConfigurationFilter.isCookieAnonymousAllow()).thenReturn(true);
        when(this.authentificationConfigurationFilter.isCookieAnonymousHttpOnly()).thenReturn(false);
        when(this.authentificationConfigurationFilter.isCookieAnonymousSecure()).thenReturn(false);

        // -->Adding HTTP headers to simulate authenticated user
        final Map<String, String> headersTrue = new HashMap<String, String>();
        headersTrue.put(IHeaderConstante.USER_GE_ACCOUNT, "eyJvcmlnaW4iOiJGQyIsImNpdmlsaXR5IjoiTSIsIm5hbWUiOiJCT0lEQVJEIn0=");
        headersTrue.put(IHeaderConstante.USER_GE_ID, "2016-11-VHB-LKD-55");
        final Enumeration enumerationHeadersTrue = new IteratorEnumeration(headersTrue.keySet().iterator());

        when(this.httpRequest.getHeader(IHeaderConstante.USER_GE_ACCOUNT)).thenReturn(headersTrue.get(IHeaderConstante.USER_GE_ACCOUNT));
        when(this.httpRequest.getHeader(IHeaderConstante.USER_GE_ID)).thenReturn(headersTrue.get(IHeaderConstante.USER_GE_ID));
        when(this.httpRequest.getHeaderNames()).thenReturn(enumerationHeadersTrue);
        when(this.httpRequest.getRequestURI()).thenReturn("/cfe/login");

        final AccountUserBean accountUserBean = new AccountUserBean();
        accountUserBean.setCivility("Monsieur");
        accountUserBean.setFirstName("Arnaud");
        accountUserBean.setLastName("BOIDARD");
        accountUserBean.setEmail("arnaud.boidard@yopmail.com");
        accountUserBean.setLanguage("fr");
        accountUserBean.setPhone("+33617243567");
        accountUserBean.setTrackerId("2016-11-VHB-LKD-55");

        when(this.accountFacade.readUser("2016-11-VHB-LKD-55")).thenReturn(accountUserBean);

        // -->Calling filter
        this.accountSecurityFilter.doFilter(this.httpRequest, this.httpResponse, this.filterChain);

        // -->Checking calls
        Mockito.verify(this.filterChain).doFilter(this.httpRequest, this.httpResponse);
        Mockito.verify(this.accountManager).cleanUserContext();
    }
}
