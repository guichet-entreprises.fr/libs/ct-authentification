/**
 *
 */
package fr.ge.ct.authentification.bean.conf;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test class of AuthentificationConfigurationFilterBean.
 *
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/applicationContext-ge-authentification-ct-web-config.xml",
  "classpath:/spring/applicationContext-ge-authentification-ct-test-config.xml" })
public class AuthentificationConfigurationFilterBeanTest {

  @Autowired
  private AuthentificationConfigurationWebBean authentificationConfigurationWebBean;

  /**
   * Init test method.
   */
  @Test
  public void initBeanTest() {
    assertEquals("urlLogout", this.authentificationConfigurationWebBean.getUrlLogout());
    assertEquals("urlModification", this.authentificationConfigurationWebBean.getUrlModification());
    assertEquals("^.*/((resources/|paiement/ipn|status?p=).*|status)$",
      this.authentificationConfigurationWebBean.getExcludeUrlsPattern().toString());
    assertEquals("http://account.int.guichet-entreprises.fr:10001/alive",
      this.authentificationConfigurationWebBean.getUrlAccountAlive());
  }

}
