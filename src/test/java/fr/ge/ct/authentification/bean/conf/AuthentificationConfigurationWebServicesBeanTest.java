/**
 *
 */
package fr.ge.ct.authentification.bean.conf;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test class of AuthentificationConfigurationWebServicesBean.
 *
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring/applicationContext-ge-authentification-ct-ws-config.xml",
  "classpath:/spring/applicationContext-ge-authentification-ct-test-config.xml" })
public class AuthentificationConfigurationWebServicesBeanTest {

  @Autowired
  private AuthentificationConfigurationWSBean authentificationConfigurationWebServicesBean;

  /**
   * Init test method.
   */
  @Test
  public void initBeanTest() {
    assertEquals("http://account.int.guichet-entreprises.fr:10001",
      this.authentificationConfigurationWebServicesBean.getUrlAccount());
  }

}
