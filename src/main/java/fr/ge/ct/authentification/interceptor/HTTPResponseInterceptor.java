/**
 * 
 */
package fr.ge.ct.authentification.interceptor;

import java.net.HttpURLConnection;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;

import fr.ge.core.log.GestionnaireTrace;

/**
 * Intercepteur des reponses CXF WebService.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class HTTPResponseInterceptor extends AbstractPhaseInterceptor {

  /** Constant for the Required SLL. */
  private static final int HTTP_REQUIRED_SSL = 426;

  /** Le logger technique. */
  private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

  /**
   * Constructeur de la classe.
   *
   */
  public HTTPResponseInterceptor() {
    super(Phase.RECEIVE);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void handleMessage(final Message message) throws Fault {

    Integer responseCode = Integer.valueOf(message.get(Message.RESPONSE_CODE).toString());

    switch (responseCode) {
      case HttpURLConnection.HTTP_BAD_REQUEST:
        LOGGER_TECH.error("AUTHENTIFICATION | Bad Request | Return Code : {}", responseCode);
        break;
      case HttpURLConnection.HTTP_NOT_FOUND:
        LOGGER_TECH.warn("AUTHENTIFICATION | Not Found | Return Code : {}", responseCode);
        break;
      case HttpURLConnection.HTTP_BAD_METHOD:
        LOGGER_TECH.error("AUTHENTIFICATION | Method Not Allowed | Return Code : {}", responseCode);
        break;
      case HTTP_REQUIRED_SSL:
        LOGGER_TECH.error("AUTHENTIFICATION | Required SSL | Return Code : {}", HTTP_REQUIRED_SSL);
        break;
      case HttpURLConnection.HTTP_INTERNAL_ERROR:
        LOGGER_TECH.error("AUTHENTIFICATION | Internal Server Error | Return Code : {}", responseCode);
        break;
      case HttpURLConnection.HTTP_UNAVAILABLE:
        LOGGER_TECH.error("AUTHENTIFICATION | Service Unavailable | Return Code : {}", responseCode);
        break;
      default:
        break;
    }

  }

}
