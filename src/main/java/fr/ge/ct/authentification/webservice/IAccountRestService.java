/**
 *
 */
package fr.ge.ct.authentification.webservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.ge.ct.authentification.bean.AccountUserBean;

/**
 * Account services contract.
 *
 * @author jpauchet
 */
public interface IAccountRestService {

    /**
     * Registers a new user.
     *
     * @param user
     *            the user to register
     * @return the user with the given id
     */
    @POST
    @Path("/private/users")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    AccountUserBean registerUser(AccountUserBean user);

    /**
     * Reads user by either Id Tracker, mail, id FC.
     *
     * @param id
     *            Id Tracker, mail, id FC
     * @return the user
     */
    @GET
    @Path("/private/users/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    AccountUserBean readUser(@PathParam("id") String id);

}
