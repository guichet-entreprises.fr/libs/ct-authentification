/**
 * 
 */
package fr.ge.ct.authentification.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

/**
 * Classe utilitaire regroupant les methodes spécifiques au filtre.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public final class FilterUtils {

  /**
   * Constructeur de la classe.
   *
   */
  private FilterUtils() {
    super();
  }

  /**
   * Exclusion des requetes qui ne respecte pas les regexp.
   *
   * @param httpRequest
   *          : la requete HTTP
   * @param patternUrlExclude
   *          la regular expression des URL qui ne doivent pas être authentifiées
   * @return : true si exclue , false sinon
   */
  public static boolean excludeFromFilter(final HttpServletRequest httpRequest, final Pattern patternUrlExclude) {
    boolean result = false;

    if (patternUrlExclude != null) {
      final String url = httpRequest.getRequestURI().toString();
      final Matcher match = patternUrlExclude.matcher(url);

      if (match.matches()) {
        result = true;
      }
    }
    return result;
  }

}
