/**
 *
 */
package fr.ge.ct.authentification.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;

import fr.ge.core.exception.TechniqueRuntimeException;
import fr.ge.ct.authentification.bean.constante.IHeaderConstante;

/**
 * Utility class to check and extract the HTTP request information.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public final class HeaderUserUtils {

  /**
   * Class constructor.
   */
  private HeaderUserUtils() {
    super();
  }

  /**
   * Instance of the Gson object.
   */
  private static Gson gson = new Gson();

  /**
   * Several checks are performed on the headers: <br>
   * Presence of the user Tracker identifier <br>
   * Presence of minimal user information.
   *
   * @param httpRequest
   *          the HttpServletRequest which contains the headers
   * @return true if all the information is present, false otherwise
   */
  public static boolean verifyHeader(final HttpServletRequest httpRequest) {
    // TODO TO IMPLEMENT
    // signature check
    return true;
  }

  /**
   * Returns a Map with the user information read from the header (encoded in base64).
   *
   * @param httpRequest
   *          the HttpServletRequest which contains the headers
   * @return returns a map of information, null if not found
   */
  public static Map < String, String > getHeadersAccountInfo(final HttpServletRequest httpRequest) {

    final String strAcccountInfoEncode = httpRequest.getHeader(IHeaderConstante.USER_GE_ACCOUNT);
    // we check the header is not empty before trying to decode it
    if (strAcccountInfoEncode == null) {
      throw new TechniqueRuntimeException(
        "Missing information in the HTTP request Header : " + IHeaderConstante.USER_GE_ACCOUNT + ", connexion refused");
    }
    final byte[] arrayAccountInfo = Base64.getDecoder().decode(strAcccountInfoEncode);
    final String strAccountInfoDecode = new String(arrayAccountInfo, StandardCharsets.UTF_8);
    return gson.fromJson(strAccountInfoDecode, Map.class);
  }

  /**
   * Returns the user identifier.
   *
   * @param httpRequest
   *          the HttpServletRequest which contains the headers
   * @return returns the identifier, null if not found
   */
  public static String getHeadersIdUser(final HttpServletRequest httpRequest) {
    final String result = httpRequest.getHeader(IHeaderConstante.USER_GE_ID);
    if (StringUtils.isBlank(result)) {
      throw new TechniqueRuntimeException(
        "Missing information in the HTTP request Header : " + IHeaderConstante.USER_GE_ID + ", connexion refused");
    }
    return result;
  }

}
