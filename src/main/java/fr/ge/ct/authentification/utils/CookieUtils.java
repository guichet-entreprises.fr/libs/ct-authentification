package fr.ge.ct.authentification.utils;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import fr.ge.core.log.GestionnaireTrace;

/**
 * Utility class to manage cookie.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public final class CookieUtils {

    /** Le logger technique. */
    private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

    /** Cookie path **/
    private static final String COOKIE_PATH = "/";

    /**
     * Constructeur de la classe.
     *
     */
    private CookieUtils() {
        super();
    }

    /**
     * Extract the cookie from HTTP request.
     * 
     * @param requestHttp
     *            The request
     * @return the Nash cookie
     */
    public static Cookie getCookie(final String cookieName, final HttpServletRequest requestHttp) {
        List<Cookie> list = Optional.ofNullable(requestHttp.getCookies()).map(Arrays::stream).orElseGet(Stream::empty).collect(Collectors.toList());
        final Cookie result = list.stream().filter(cookie -> (cookieName.equals(cookie.getName()))).findFirst().orElse(null);
        if (null == result || StringUtils.isEmpty(result.getValue())) {
            LOGGER_TECH.debug("No cookie found " + cookieName + " in the request " + requestHttp.getRequestURI());
        }
        return result;
    }

    /**
     * Creates a cookie containing anonymous uid.
     * 
     * @param httpRequest
     *            the HTTP request
     * @param value
     *            The value to be hashed
     * @param cookieAnonymousSecure
     *            use secure protocol
     * @param cookieAnonymousHttpOnly
     *            mark cookie as HttpOnly
     * @return
     */
    public static Cookie createCookie(final HttpServletRequest httpRequest, final String name, final String value, final int cookieAnonymousAge, final boolean cookieAnonymousSecure,
            final boolean cookieAnonymousHttpOnly) {
        // String trimedName = name.substring(1);
        Cookie cookie = new Cookie(name, hash(value));
        cookie.setSecure(cookieAnonymousSecure);
        cookie.setHttpOnly(cookieAnonymousHttpOnly);
        cookie.setMaxAge(cookieAnonymousAge);
        cookie.setPath(COOKIE_PATH);
        CookieUtils.addCookieDomain(httpRequest, cookie);
        LOGGER_TECH.debug("Creating cookie {} ({})", value, cookie.getDomain());
        return cookie;
    }

    /**
     * Generate an identifier.
     * 
     * @return
     */
    public static String uid() {
        return new UidFactoryImpl().uid();
    }

    /**
     * Setting cookie from HTTP request.
     * 
     * @param request
     *            The HTTP request
     * @param cookie
     *            the cookie input
     */
    public static void addCookieDomain(final HttpServletRequest request, final Cookie cookie) {
        String domain = request.getServerName();
        if (domain != null) {
            int index = domain.indexOf(".");
            if (index >= 0) {
                // DEST-423 Utiliser des noms de domaine dans les cookies sans
                // "." en
                // préfixe dans Tomcat 8 et 9
                domain = domain.substring(index + 1, domain.length());
                cookie.setDomain(domain);
            }
        }
        LOGGER_TECH.info("Cookie domain {} : {} - Secure : {} - value : {} - path : {}", cookie.getName(), domain, cookie.getSecure(), cookie.getValue(), cookie.getPath());
    }

    /**
     * Return input value using Base64 encoding.
     *
     * @param value
     *            the input value to hash
     * @return the SHA-1 using string
     */
    public static String hash(final String value) {
        return new String(Base64.getEncoder().encode(value.getBytes(StandardCharsets.UTF_8)));
    }
}
