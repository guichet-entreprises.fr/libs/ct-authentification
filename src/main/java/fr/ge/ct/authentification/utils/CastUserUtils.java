/**
 * 
 */
package fr.ge.ct.authentification.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import fr.ge.core.bean.GeUserBean;
import fr.ge.ct.authentification.bean.json.JsonUserBean;

/**
 * Utility Class for Casting User.
 * 
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public final class CastUserUtils {

  /** Pattern date. **/
  private static final String DATE_CONNECTION_PATTERN = "yyyy-MM-dd HH:mm";

  /** Date format date. **/
  private static final DateFormat DATE_CONNECTION_FORMAT = new SimpleDateFormat(DATE_CONNECTION_PATTERN);

  /**
   * Constructor of the class.
   *
   */
  private CastUserUtils() {
    super();
  }

  /**
   * Cast JsonUser to GeUserBean.
   * 
   * @param user
   *          : Json User
   * @return GeUserBean
   */
  public static GeUserBean castToGeUserBean(final JsonUserBean user) {
    GeUserBean geUserBean = new GeUserBean(user.getId());
    geUserBean.setCivility(user.getCivility());
    geUserBean.setName(user.getName());
    geUserBean.setForName(user.getForName());
    geUserBean.setMail(user.getMail());
    geUserBean.setLanguage(user.getLanguage());
    geUserBean.setPhone(user.getPhone());
    geUserBean.setValidPhone(Boolean.valueOf(user.getPhoneCheck()));
    try {
      final String connexionDate = user.getConnexionDate();
      geUserBean.setConnexionDate(DATE_CONNECTION_FORMAT.parse(connexionDate));
    } catch (ParseException e) {
      // -->Cas passant
    }
    return geUserBean;
  }

}
