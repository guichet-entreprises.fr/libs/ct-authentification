package fr.ge.ct.authentification.facade;

import fr.ge.core.exception.TechniqueException;
import fr.ge.ct.authentification.bean.AccountUserBean;

/**
 * Account services facade.<br/>
 * All methods throws a RuntimeException.
 *
 * @author jpauchet
 */
public interface IAccountFacade {

    /**
     * Registers a new user.
     *
     * @param userId
     *            id of a user
     * @return the User with the given id
     * @throws TechniqueException
     *             : TechniqueException if code HTTP is different 200 or if
     *             problem for parsing response{@ link ICodeExceptionConstante}
     */
    AccountUserBean registerUser(final AccountUserBean user);

    /**
     * Reads user by either Id Tracker, mail, id FC.
     *
     * @param id
     *            Id Tracker, mail, id FC
     * @return the user
     */
    AccountUserBean readUser(String id);

}
