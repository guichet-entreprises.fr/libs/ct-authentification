/**
 *
 */
package fr.ge.ct.authentification.facade.impl;

import java.net.HttpURLConnection;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import fr.ge.core.exception.TechniqueException;
import fr.ge.core.exception.TechniqueRuntimeException;
import fr.ge.core.log.GestionnaireTrace;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.bean.IErrorReadyUser;
import fr.ge.ct.authentification.bean.constante.ICodeExceptionConstante;
import fr.ge.ct.authentification.facade.IAccountFacade;
import fr.ge.ct.authentification.webservice.IAccountRestService;

/**
 * Account services facade.
 *
 * @author jpauchet
 */
public class AccountFacadeImpl implements IAccountFacade {

    /** Account rest service. */
    private IAccountRestService accountRestService;

    /** The functionnal logger. */
    private static final Logger LOGGER_FONC = GestionnaireTrace.getLoggerFonctionnel();

    /**
     * {@inheritDoc}
     */
    @Override
    public AccountUserBean registerUser(final AccountUserBean user) {
        try {
            final AccountUserBean registeredUser = this.accountRestService.registerUser(user);
            return registeredUser;
        } catch (TechniqueRuntimeException e) {
            LOGGER_FONC.error(String.format("Unable to register this user : %s.", user.getEmail()), e);
            throw e;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AccountUserBean readUser(final String id) {
        try {
            final AccountUserBean user = this.accountRestService.readUser(id);
            return user;
        } catch (Exception e) {
            LOGGER_FONC.error(String.format("Unable to read this user : %s.", id), e);
            throw new TechniqueRuntimeException(String.format("Unable to read this user : %s.", id), e);
        }
    }

    /**
     * Checks the result of the response is well formatted.
     *
     * @param response
     *            Response of web service
     * @param userId
     *            (parameter of the webservice), needed for log
     * @return the user read from the response
     * @throws TechniqueException
     *             the technique exception
     */
    private <T extends IErrorReadyUser> T checkJsonUserResponse(final Response response, final String userId, final Class<T> responseType) throws TechniqueException {
        if (HttpURLConnection.HTTP_OK != response.getStatus()) {
            throw new TechniqueException(ICodeExceptionConstante.WS_AUTH_ERROR_CONNECTION, "Not able to get informations of the user : " + userId + " | Status Response : " + response.getStatus());
        }
        T user = null;
        try {
            user = response.readEntity(responseType);
        } catch (final Exception e) {
            LOGGER_FONC.error("Not able to deserialize the user", e);
            throw new TechniqueException(ICodeExceptionConstante.WS_AUTH_ERROR_PARSING, "Not able to deserialize the user : " + userId, e);
        }
        if (StringUtils.isNotEmpty(user.getErrorCode())) {
            LOGGER_FONC.info("Not able to get informations of the user : " + userId + " | Error : " + user.getErrorCode() + ", " + user.getErrorMessage());
        }
        return user;
    }

    /**
     * Sets the account rest service.
     *
     * @param accountRestService
     *            the new account rest service
     */
    public void setAccountRestService(final IAccountRestService accountRestService) {
        this.accountRestService = accountRestService;
    }

}
