/**
 *
 */
package fr.ge.ct.authentification.manager;

import fr.ge.core.bean.UserBean;
import fr.ge.ct.authentification.bean.AccountUserBean;

/**
 * AccountManager interface to implement for integrating the
 * ct-authentification.
 *
 * @param <T>
 *            : Bean that must extend the UserBean
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public interface IAccountManager<T extends UserBean> {

    /**
     * Builds the UserBean from the input parameters specific to each
     * application.
     *
     * @param idUser
     *            user identifier
     * @param mapInfoUser
     *            map which contains the user account information
     * @return UserBean which depends on the input parameters
     */
    T getUserBean(String idUser, final AccountUserBean accountUserBean);

    /**
     * Persists the UserBean in the context.
     *
     * @param userBean
     *            object which contains the user account information
     */
    void persistUserContext(T userBean);

    /**
     * Empties the user context.
     */
    void cleanUserContext();

}
