/**
 *
 */
package fr.ge.ct.authentification.filter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.GenericFilterBean;

import fr.ge.core.exception.TechniqueRuntimeException;
import fr.ge.ct.authentification.bean.AccountUserBean;
import fr.ge.ct.authentification.bean.conf.AuthentificationConfigurationWebBean;
import fr.ge.ct.authentification.bean.constante.IHeaderConstante;
import fr.ge.ct.authentification.facade.IAccountFacade;
import fr.ge.ct.authentification.manager.IAccountManager;
import fr.ge.ct.authentification.utils.CookieUtils;
import fr.ge.ct.authentification.utils.FilterUtils;
import fr.ge.ct.authentification.utils.HeaderUserUtils;

/**
 * The servlet filter for the user account. Allows to retrieve the identifier
 * (Tracker) and the account information. <br>
 * If data are missing, a 401 HTTP code is returned.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public class AccountSecurityFilter extends GenericFilterBean {

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountSecurityFilter.class);

    /** AccountManager interface. */
    @Autowired
    private IAccountManager accountManager;

    /** Application Account façade */
    @Autowired
    private IAccountFacade accountFacade;

    /** Bean for loading the authentication configuration. */
    @Autowired
    private AuthentificationConfigurationWebBean authentificationConfigurationWebBean;

    /**
     * Civité de l'utilisateur.
     */
    String USER_GE_ACCOUNT_CIVILITY = "civility";

    /**
     * Nom de l'utilisateur.
     */
    String USER_GE_ACCOUNT_NAME = "name";

    /**
     * Prénom de l'utilisateur.
     */
    String USER_GE_ACCOUNT_FORNAME = "forName";

    /**
     * Clé pour le email de l'utilisateur.
     */
    String USER_GE_ACCOUNT_MAIL = "mail";

    /**
     * Clé pour le langage de l'utilisateur.
     */
    String USER_GE_ACCOUNT_LANGUAGE = "language";

    /**
     * Clé pour le telephone de l'utilisateur.
     */
    String USER_GE_ACCOUNT_PHONE = "phone";

    /**
     * Clé pour la validite du telephone de l'utilisateur.
     */
    String USER_GE_ACCOUNT_PHONE_CHECK = "phoneCheck";

    /**
     * {@inheritDoc}
     */
    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain) throws IOException, ServletException {

        if (!(servletRequest instanceof HttpServletRequest) || !(servletResponse instanceof HttpServletResponse)) {
            throw new TechniqueRuntimeException("Only HTTP protocol supported. Connection refused.");
        }

        final HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        final HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

        // Exclusion of some URLs (see excludeUrls)
        if (excludeFromFilter(httpRequest)) {
            filterChain.doFilter(httpRequest, httpResponse);
            return;
        }

        // we check the signature and if it is not valid, a 401 HTTP code is
        // returned
        if (!HeaderUserUtils.verifyHeader(httpRequest)) {
            // if a piece of information is missing, a 401 HTTP code is returned
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "HTTP request signature not conform, connexion refused");
            return;
        }

        try {
            AccountUserBean userBean = new AccountUserBean();
            // Map<String, String> accountInfoUser = null;
            String userId = null;
            Map<String, String> accountInfoUser = new HashMap<String, String>();
            if (authentificationConfigurationWebBean.isCookieAnonymousAllow() && StringUtils.isBlank(httpRequest.getHeader(IHeaderConstante.USER_GE_ID))) {
                // -->Checking if the anonymous cookie is present
                final Cookie anonymousCookie = CookieUtils.getCookie(authentificationConfigurationWebBean.getCookieAnonymousName(), httpRequest);

                // -->User no authenticated
                if (null != anonymousCookie) {
                    userId = new String(Base64.getDecoder().decode(anonymousCookie.getValue()), StandardCharsets.UTF_8);
                } else {
                    // -->Create an anonymous cookie
                    userId = CookieUtils.uid();
                    final Cookie cookie = CookieUtils.createCookie(httpRequest, authentificationConfigurationWebBean.getCookieAnonymousName(), userId,
                            authentificationConfigurationWebBean.getCookieAnonymousAge(), authentificationConfigurationWebBean.isCookieAnonymousSecure(),
                            authentificationConfigurationWebBean.isCookieAnonymousHttpOnly());
                    httpResponse.addCookie(cookie);
                }

                // DEST-626 Set anonymous variable to true
                userBean.setAnonymous(true);

            } else {
                userId = HeaderUserUtils.getHeadersIdUser(httpRequest);
                // -->MINE-266 Allow user to connect to nash-support without
                // authenticated user
                // get user informations from account or headers
                accountInfoUser = HeaderUserUtils.getHeadersAccountInfo(httpRequest);
                if (authentificationConfigurationWebBean.isTrustInHeaders() && StringUtils.isNotBlank(accountInfoUser.get(USER_GE_ACCOUNT_CIVILITY))
                        && StringUtils.isNotBlank(accountInfoUser.get(USER_GE_ACCOUNT_FORNAME)) && StringUtils.isNotBlank(accountInfoUser.get(USER_GE_ACCOUNT_MAIL))
                        && StringUtils.isNotBlank(accountInfoUser.get(USER_GE_ACCOUNT_NAME)) && StringUtils.isNotBlank(accountInfoUser.get(USER_GE_ACCOUNT_LANGUAGE))) {
                    userBean.setCivility(accountInfoUser.get(USER_GE_ACCOUNT_CIVILITY));
                    userBean.setFirstName(accountInfoUser.get(USER_GE_ACCOUNT_FORNAME));
                    userBean.setLastName(accountInfoUser.get(USER_GE_ACCOUNT_NAME));
                    userBean.setEmail(accountInfoUser.get(USER_GE_ACCOUNT_MAIL));
                    userBean.setPhone(accountInfoUser.get(USER_GE_ACCOUNT_PHONE));
                    userBean.setPhoneCheck(false);
                    userBean.setLanguage(accountInfoUser.get(USER_GE_ACCOUNT_LANGUAGE));
                    Boolean validPhone = Boolean.valueOf(((String) accountInfoUser.get(USER_GE_ACCOUNT_PHONE_CHECK)));
                    if (validPhone == null) {
                        validPhone = false;
                    }
                    userBean.setPhoneCheck(validPhone);
                } else {
                    // get user informations from account
                    userBean = accountFacade.readUser(userId);
                }
                // <--
                if (null != userBean) {
                    userBean.setAnonymous(false);
                }
            }

            // Persistence in the thread local
            accountManager.persistUserContext(accountManager.getUserBean(userId, userBean));

            filterChain.doFilter(httpRequest, httpResponse);

        } catch (final TechniqueRuntimeException e) {
            // if a piece of information is missing, a 401 HTTP code is returned
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Missing information in the HTTP request header");
        } finally {
            // we empty the context
            accountManager.cleanUserContext();
        }

    }

    /**
     * Exclusion of requests defined in the calling webapp. For these requests,
     * the authentication check is not necessary.
     *
     * @param httpRequest
     *            the HTTP request
     * @return true if excluded , false otherwise
     */
    private boolean excludeFromFilter(final HttpServletRequest httpRequest) {

        return FilterUtils.excludeFromFilter(httpRequest, authentificationConfigurationWebBean.getExcludeUrlsPattern());
    }

}
