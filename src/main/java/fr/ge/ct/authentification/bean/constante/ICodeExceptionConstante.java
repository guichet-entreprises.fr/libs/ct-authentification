/**
 *
 */
package fr.ge.ct.authentification.bean.constante;

/**
 * Constant listing the keys of exception provided by this project.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public interface ICodeExceptionConstante {

  /**
   * Key for error connection.
   */
  String WS_AUTH_ERROR_CONNECTION = "WS_AUTH_ERROR_CONNECTION";

  /**
   * Key for error parsing the body.
   */
  String WS_AUTH_ERROR_PARSING = "WS_AUTH_ERROR_PARSING";
}
