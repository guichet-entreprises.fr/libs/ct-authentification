/**
 *
 */
package fr.ge.ct.authentification.bean;

/**
 * Defines a user bean as being able to handle errors.
 *
 * @author jpauchet
 */
public interface IErrorReadyUser {

    /**
     * Gets the error code.
     *
     * @return the error code
     */
    String getErrorCode();

    /**
     * Gets the error message.
     *
     * @return the error message
     */
    String getErrorMessage();

}
