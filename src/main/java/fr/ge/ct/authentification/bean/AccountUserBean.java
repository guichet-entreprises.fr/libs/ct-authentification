/**
 *
 */
package fr.ge.ct.authentification.bean;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Account user bean.
 *
 * @author jpauchet
 */
public class AccountUserBean implements IErrorReadyUser {

    /** The first name. */
    private String firstName;

    /** The last name. */
    private String lastName;

    /** The civility. */
    private String civility;

    /** The language. */
    private String language;

    /** The email. */
    private String email;

    /** The password. */
    private String password;

    /** The phone. */
    private String phone;

    /** The Tracker id. */
    private String trackerId;

    /** The FranceConnect id. **/
    private String fcId;

    /** The phone check. */
    private boolean phoneCheck;

    /** The connection date. */
    private String connectionDate;

    /** The mail type. */
    private String mailType;

    /** Error code. */
    private String errorCode;

    /** Error message. */
    private String errorMessage;

    /**
     * Anonymous user (not registred in account </br>
     * Used for nash instances that allows anonymous users
     */
    private boolean anonymous;

    /**
     * Constructor.
     *
     */
    public AccountUserBean() {
        super();
    }

    /**
     * Constructor.
     *
     * @param firstName
     *            the first name
     * @param lastName
     *            the last name
     * @param civility
     *            the civility
     * @param language
     *            the language
     * @param email
     *            the email
     * @param phone
     *            the phone
     * @param mailType
     *            the mailType
     */
    public AccountUserBean(final String firstName, final String lastName, final String civility, final String language, final String email, final String phone, final String mailType) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.civility = civility;
        this.language = language;
        this.email = email;
        this.phone = phone;
        this.mailType = mailType;
    }

    /**
     * Constructor.
     *
     * @param errorCode
     *            the error code
     * @param errorMessage
     *            the error message
     */
    public AccountUserBean(final String errorCode, final String errorMessage) {
        super();
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    /**
     * Gets the first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Sets the first name.
     *
     * @param firstName
     *            the new first name
     * @return the account register user bean
     */
    public AccountUserBean setFirstName(final String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Gets the last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Sets the last name.
     *
     * @param lastName
     *            the new last name
     * @return the account register user bean
     */
    public AccountUserBean setLastName(final String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * Gets the civility.
     *
     * @return the civility
     */
    public String getCivility() {
        return this.civility;
    }

    /**
     * Sets the civility.
     *
     * @param civility
     *            the new civility
     * @return the account register user bean
     */
    public AccountUserBean setCivility(final String civility) {
        this.civility = civility;
        return this;
    }

    /**
     * Gets the language.
     *
     * @return the language
     */
    public String getLanguage() {
        return this.language;
    }

    /**
     * Sets the language.
     *
     * @param language
     *            the new language
     * @return the account register user bean
     */
    public AccountUserBean setLanguage(final String language) {
        this.language = language;
        return this;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Sets the email.
     *
     * @param email
     *            the new email
     * @return the account register user bean
     */
    public AccountUserBean setEmail(final String email) {
        this.email = email;
        return this;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Sets the password.
     *
     * @param password
     *            the new password
     * @return the account register user bean
     */
    public AccountUserBean setPassword(final String password) {
        this.password = password;
        return this;
    }

    /**
     * Gets the phone.
     *
     * @return the phone
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * Sets the phone.
     *
     * @param phone
     *            the new phone
     * @return the account register user bean
     */
    public AccountUserBean setPhone(final String phone) {
        this.phone = phone;
        return this;
    }

    /**
     * Gets the tracker id.
     *
     * @return the tracker id
     */
    public String getTrackerId() {
        return this.trackerId;
    }

    /**
     * Sets the tracker id.
     *
     * @param trackerId
     *            the new tracker id
     * @return the account register user bean
     */
    public AccountUserBean setTrackerId(final String trackerId) {
        this.trackerId = trackerId;
        return this;
    }

    /**
     * Gets the fc id.
     *
     * @return the fc id
     */
    public String getFcId() {
        return this.fcId;
    }

    /**
     * Sets the fc id.
     *
     * @param fcId
     *            the new fc id
     * @return the account register user bean
     */
    public AccountUserBean setFcId(final String fcId) {
        this.fcId = fcId;
        return this;
    }

    /**
     * Gets the phone check.
     *
     * @return the phone check
     */
    public boolean getPhoneCheck() {
        return this.phoneCheck;
    }

    /**
     * Sets the phone check.
     *
     * @param phoneCheck
     *            the new phone check
     * @return the account register user bean
     */
    public AccountUserBean setPhoneCheck(final boolean phoneCheck) {
        this.phoneCheck = phoneCheck;
        return this;
    }

    /**
     * Gets the connection date.
     *
     * @return the connection date
     */
    public String getConnectionDate() {
        return this.connectionDate;
    }

    /**
     * Sets the connection date.
     *
     * @param connectionDate
     *            the new connection date
     * @return the account register user bean
     */
    public AccountUserBean setConnectionDate(final String connectionDate) {
        this.connectionDate = connectionDate;
        return this;
    }

    /**
     * Gets the mail type.
     *
     * @return the mail type
     */
    public String getMailType() {
        return this.mailType;
    }

    /**
     * Sets the mail type.
     *
     * @param mailType
     *            the new mail type
     * @return the account register user bean
     */
    public AccountUserBean setMailType(final String mailType) {
        this.mailType = mailType;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public String getErrorCode() {
        return this.errorCode;
    }

    /**
     * {@inheritDoc}
     */
    public AccountUserBean setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    /**
     * Gets the error message.
     *
     * @return the error message
     */
    public String getErrorMessage() {
        return this.errorMessage;
    }

    /**
     * Sets the error message.
     *
     * @param errorMessage
     *            the new error message
     * @return the account register user bean
     */
    public AccountUserBean setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    /**
     * @return the anonymous
     */
    public boolean isAnonymous() {
        return anonymous;
    }

    /**
     * @param anonymous
     *            the anonymous to set
     */
    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

}
