/**
 *
 */
package fr.ge.ct.authentification.bean.conf;

import java.io.Serializable;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.regex.Pattern;

/**
 * AuthentificationConfigurationFilterBean which contains different URLs.
 *
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
public class AuthentificationConfigurationWebBean implements Serializable {

    /** The serialVersionUID constant. */
    private static final long serialVersionUID = -1866859830898012512L;

    /** The logout URL. */
    private String urlLogout;

    /** The account modification URL. */
    private String urlModification;

    /** The URL that check if the account module is alive. */
    private String urlAccountAlive;

    /**
     * Compiled representation of the regular expression which depicts the list
     * of URLs to exclude on the authentication check filter.
     */
    private Pattern excludeUrlsPattern;

    /**
     * Anonymous cookie name.
     */
    private String cookieAnonymousName;

    /**
     * Allow anonymous session.
     */
    private boolean cookieAnonymousAllow;

    /**
     * Anonymous cookie age.
     */
    private int cookieAnonymousAge;

    /**
     * Allow anonymous session.
     */
    private boolean cookieAnonymousSecure;

    /**
     * Allow anonymous session.
     */
    private boolean cookieAnonymousHttpOnly;

    /**
     * Trust the user informations in the header.
     */
    private boolean trustInHeaders;

    /** Static instance of the AuthentificationConfiguration bean. */
    private static AuthentificationConfigurationWebBean instance = new AuthentificationConfigurationWebBean();

    /**
     * Retrieves the unique instance of the
     * AuthentificationConfigurationFilterBean.
     *
     * @return the unique instance of the
     *         AuthentificationConfigurationFilterBean
     */
    public static AuthentificationConfigurationWebBean getInstance() {
        return instance;
    }

    /**
     * Getter on attribute {@link #urlLogout}.
     *
     * @return String urlLogout
     */
    public String getUrlLogout() {
        return this.urlLogout;
    }

    /**
     * Setter on attribute {@link #urlLogout}.
     *
     * @param urlLogout
     *            the new value of attribute urlLogout
     */
    public void setUrlLogout(final String urlLogout) {
        this.urlLogout = urlLogout;
    }

    /**
     * Getter on attribute {@link #urlModification}.
     *
     * @return String urlModification
     */
    public String getUrlModification() {
        return this.urlModification;
    }

    /**
     * Setter on attribute {@link #urlModification}.
     *
     * @param urlModification
     *            the new value of attribute urlModification
     */
    public void setUrlModification(final String urlModification) {
        this.urlModification = urlModification;
    }

    /**
     * Getter on attribute {@link #excludeUrlsPattern}.
     *
     * @return Pattern excludeUrlsPattern
     */
    public Pattern getExcludeUrlsPattern() {
        return this.excludeUrlsPattern;
    }

    /**
     * Setter on attribute {@link #excludeUrlsPattern}.
     *
     * @param excludeUrlsPattern
     *            the new value of attribute excludeUrlsPattern
     */
    public void setExcludeUrlsPattern(final Pattern excludeUrlsPattern) {
        this.excludeUrlsPattern = excludeUrlsPattern;
    }

    /**
     * Accesseur sur l'attribut {@link #urlAccountAlive}.
     *
     * @return String urlAccountAlive
     */
    public String getUrlAccountAlive() {
        return urlAccountAlive;
    }

    /**
     * Mutateur sur l'attribut {@link #urlAccountAlive}.
     *
     * @param urlAccountAlive
     *            la nouvelle valeur de l'attribut urlAccountAlive
     */
    public void setUrlAccountAlive(final String urlAccountAlive) {
        this.urlAccountAlive = urlAccountAlive;
    }

    /**
     * Return the Account logout url with the callback url.
     * 
     * @param callbackUrl
     *            The callback url
     * @return The complete Account logout url
     */
    public String getUrlLogoutCallback(final String callbackUrl) {
        try {
            return this.urlLogout + URLEncoder.encode(callbackUrl, StandardCharsets.UTF_8.toString());
        } catch (Exception e) {
            return this.urlLogout;
        }
    }

    /**
     * Return the Account modification url with the callback url.
     * 
     * @param callbackUrl
     *            The callback url
     * @return The complete Account logout url
     */
    public String getUrlModificationCallback(final String callbackUrl) {
        try {
            return this.urlModification + URLEncoder.encode(callbackUrl, StandardCharsets.UTF_8.toString());
        } catch (Exception e) {
            return this.urlModification;
        }
    }

    /**
     * Accesseur sur l'attribut {@link #cookieAnonymousAllow}.
     *
     * @return boolean cookieAnonymousAllow
     */
    public boolean isCookieAnonymousAllow() {
        return cookieAnonymousAllow;
    }

    /**
     * Mutateur sur l'attribut {@link #cookieAnonymousAllow}.
     *
     * @param cookieAnonymousAllow
     *            la nouvelle valeur de l'attribut cookieAnonymousAllow
     */
    public void setCookieAnonymousAllow(final boolean cookieAnonymousAllow) {
        this.cookieAnonymousAllow = cookieAnonymousAllow;
    }

    /**
     * Accesseur sur l'attribut {@link #cookieAnonymousSecure}.
     *
     * @return boolean cookieAnonymousSecure
     */
    public boolean isCookieAnonymousSecure() {
        return cookieAnonymousSecure;
    }

    /**
     * Mutateur sur l'attribut {@link #cookieAnonymousSecure}.
     *
     * @param cookieAnonymousSecure
     *            la nouvelle valeur de l'attribut cookieAnonymousSecure
     */
    public void setCookieAnonymousSecure(final boolean cookieAnonymousSecure) {
        this.cookieAnonymousSecure = cookieAnonymousSecure;
    }

    /**
     * Accesseur sur l'attribut {@link #cookieAnonymousHttpOnly}.
     *
     * @return boolean cookieAnonymousHttpOnly
     */
    public boolean isCookieAnonymousHttpOnly() {
        return cookieAnonymousHttpOnly;
    }

    /**
     * Mutateur sur l'attribut {@link #cookieAnonymousHttpOnly}.
     *
     * @param cookieAnonymousHttpOnly
     *            la nouvelle valeur de l'attribut cookieAnonymousHttpOnly
     */
    public void setCookieAnonymousHttpOnly(final boolean cookieAnonymousHttpOnly) {
        this.cookieAnonymousHttpOnly = cookieAnonymousHttpOnly;
    }

    /**
     * Accesseur sur l'attribut {@link #cookieAnonymousAge}.
     *
     * @return int cookieAnonymousAge
     */
    public int getCookieAnonymousAge() {
        return cookieAnonymousAge;
    }

    /**
     * Mutateur sur l'attribut {@link #cookieAnonymousAge}.
     *
     * @param cookieAnonymousAge
     *            la nouvelle valeur de l'attribut cookieAnonymousAge
     */
    public void setCookieAnonymousAge(final int cookieAnonymousAge) {
        this.cookieAnonymousAge = cookieAnonymousAge;
    }

    /**
     * Accesseur sur l'attribut {@link #cookieAnonymousName}.
     *
     * @return String cookieAnonymousName
     */
    public String getCookieAnonymousName() {
        return cookieAnonymousName;
    }

    /**
     * Mutateur sur l'attribut {@link #cookieAnonymousName}.
     *
     * @param cookieAnonymousName
     *            la nouvelle valeur de l'attribut cookieAnonymousName
     */
    public void setCookieAnonymousName(String cookieAnonymousName) {
        this.cookieAnonymousName = cookieAnonymousName;
    }

    /**
     * @return the trustInHeaders
     */
    public boolean isTrustInHeaders() {
        return trustInHeaders;
    }

    /**
     * @param trustInHeaders
     *            the trustInHeaders to set
     */
    public void setTrustInHeaders(boolean trustInHeaders) {
        this.trustInHeaders = trustInHeaders;
    }

}
