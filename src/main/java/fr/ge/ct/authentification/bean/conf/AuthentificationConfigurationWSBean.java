/**
 *
 */
package fr.ge.ct.authentification.bean.conf;

import java.io.Serializable;

/**
 * AuthentificationConfigurationWebServicesBean which contains different URLs.
 *
 * @author $Author: jzaire $
 * @version $Revision: 0 $
 */
public class AuthentificationConfigurationWSBean implements Serializable {

  /** The serialVersionUID constant. */
  private static final long serialVersionUID = -1866859830898012512L;

  /** The URL of the WebService. */
  private String urlAccount;

  /** Static instance of the AuthentificationConfigurationWebService bean. */
  private static AuthentificationConfigurationWSBean instance = new AuthentificationConfigurationWSBean();

  /**
   * Retrieves the unique instance of the AuthentificationConfigurationWebServicesBean.
   *
   * @return the unique instance of the AuthentificationConfigurationWebServicesBean
   */
  public static AuthentificationConfigurationWSBean getInstance() {
    return instance;
  }

  /**
   * Accesseur sur l'attribut {@link #urlAccount}.
   *
   * @return String urlAccountAlive
   */
  public String getUrlAccount() {
    return urlAccount;
  }

  /**
   * Mutateur sur l'attribut {@link #urlAccount}.
   *
   * @param urlAccount
   *          la nouvelle valeur de l'attribut urlAccount
   */
  public void setUrlAccount(final String urlAccount) {
    this.urlAccount = urlAccount;
  }

}
