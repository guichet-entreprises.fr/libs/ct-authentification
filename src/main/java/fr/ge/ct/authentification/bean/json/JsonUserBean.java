/**
 *
 */
package fr.ge.ct.authentification.bean.json;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.ge.core.log.GestionnaireTrace;

/**
 * The Class Reference.
 */
public class JsonUserBean {

    /** Le logger technique. */
    private static final Logger LOGGER_TECH = GestionnaireTrace.getLoggerTechnique();

    /** The id. */
    private String id;

    /** The civility. */
    private String civility;

    /** The name. */
    private String name;

    /** The forName. */
    private String forName;

    /** The mail. */
    private String mail;

    /** The language. */
    private String language;

    /** The phone. */
    private String phone;

    /** The phone. */
    private String rescueMail;

    /** The phone. */
    private String phoneCheck;

    /** The last date connection. **/
    private String connexionDate;

    /** Error code. */
    private String errorCode;

    /** Message. */
    private String message;

    /**
     * Constructor of the class.
     *
     */
    public JsonUserBean() {
        super();
    }

    /**
     * Constructor of the class.
     *
     * @param jsonString
     *            : a string containing the json of the user
     * @throws Exception
     *             : exception due to parsing problem, caught at the next level!
     */
    public JsonUserBean(final String jsonString) throws Exception {
        super();
        LOGGER_TECH.info("Json string value : {}", jsonString);
        final ObjectMapper mapper = new ObjectMapper();
        final JsonUserBean resultUser = mapper.readValue(jsonString, JsonUserBean.class);
        this.id = resultUser.getId();
        this.civility = resultUser.getCivility();
        this.name = resultUser.getName();
        this.forName = resultUser.getForName();
        this.mail = resultUser.getMail();
        this.language = resultUser.getLanguage();
        this.phone = resultUser.getPhone();
        this.rescueMail = resultUser.getRescueMail();
        this.phoneCheck = resultUser.getPhoneCheck();
        this.connexionDate = resultUser.getConnexionDate();
        this.errorCode = resultUser.getErrorCode();
        this.message = resultUser.getMessage();
    }

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return String id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Accesseur sur l'attribut {@link #civility}.
     *
     * @return String civility
     */
    public String getCivility() {
        return this.civility;
    }

    /**
     * Mutateur sur l'attribut {@link #civility}.
     *
     * @param civility
     *            la nouvelle valeur de l'attribut civility
     */
    public void setCivility(final String civility) {
        this.civility = civility;
    }

    /**
     * Accesseur sur l'attribut {@link #name}.
     *
     * @return String name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Mutateur sur l'attribut {@link #name}.
     *
     * @param name
     *            la nouvelle valeur de l'attribut name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Accesseur sur l'attribut {@link #forName}.
     *
     * @return String forName
     */
    public String getForName() {
        return this.forName;
    }

    /**
     * Mutateur sur l'attribut {@link #forName}.
     *
     * @param forName
     *            la nouvelle valeur de l'attribut forName
     */
    public void setForName(final String forName) {
        this.forName = forName;
    }

    /**
     * Accesseur sur l'attribut {@link #mail}.
     *
     * @return String mail
     */
    public String getMail() {
        return this.mail;
    }

    /**
     * Accesseur sur l'attribut {@link #phoneCheck}.
     *
     * @return String phoneCheck
     */
    public String getPhoneCheck() {
        return this.phoneCheck;
    }

    /**
     * Mutateur sur l'attribut {@link #phoneCheck}.
     *
     * @param phoneCheck
     *            la nouvelle valeur de l'attribut phoneCheck
     */
    public void setPhoneCheck(final String phoneCheck) {
        this.phoneCheck = phoneCheck;
    }

    /**
     * Mutateur sur l'attribut {@link #mail}.
     *
     * @param mail
     *            la nouvelle valeur de l'attribut mail
     */
    public void setMail(final String mail) {
        this.mail = mail;
    }

    /**
     * Accesseur sur l'attribut {@link #language}.
     *
     * @return String language
     */
    public String getLanguage() {
        return this.language;
    }

    /**
     * Mutateur sur l'attribut {@link #language}.
     *
     * @param language
     *            la nouvelle valeur de l'attribut language
     */
    public void setLanguage(final String language) {
        this.language = language;
    }

    /**
     * Accesseur sur l'attribut {@link #phone}.
     *
     * @return String phone
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * Accesseur sur l'attribut {@link #rescueMail}.
     *
     * @return String rescueMail
     */
    public String getRescueMail() {
        return this.rescueMail;
    }

    /**
     * Mutateur sur l'attribut {@link #rescueMail}.
     *
     * @param rescueMail
     *            la nouvelle valeur de l'attribut rescueMail
     */
    public void setRescueMail(final String rescueMail) {
        this.rescueMail = rescueMail;
    }

    /**
     * Mutateur sur l'attribut {@link #phone}.
     *
     * @param phone
     *            la nouvelle valeur de l'attribut phone
     */
    public void setPhone(final String phone) {
        this.phone = phone;
    }

    /**
     * Accesseur sur l'attribut {@link #connexionDate}.
     *
     * @return String connexionDate
     */
    public String getConnexionDate() {
        return this.connexionDate;
    }

    /**
     * Mutateur sur l'attribut {@link #connexionDate}.
     *
     * @param connexionDate
     *            la nouvelle valeur de l'attribut connexionDate
     */
    public void setConnexionDate(final String connexionDate) {
        this.connexionDate = connexionDate;
    }

    /**
     * Gets the error code.
     *
     * @return the error code
     */
    public String getErrorCode() {
        return this.errorCode;
    }

    /**
     * Sets the error code.
     *
     * @param errorCode
     *            the new error code
     */
    public void setErrorCode(final String errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Sets the message.
     *
     * @param message
     *            the new message
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        } else {
            return EqualsBuilder.reflectionEquals(this, obj);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "JsonUserBean [id=" + this.id + ", civility=" + this.civility + ", name=" + this.name + ", forName=" + this.forName + ", mail=" + this.mail + ", language=" + this.language + ", phone="
                + this.phone + ", rescueMail=" + this.rescueMail + ", phoneCheck=" + this.phoneCheck + ", connexionDate=" + this.connexionDate + "]";
    }

}
