/**
 *
 */
package fr.ge.ct.authentification.bean.constante;

/**
 * Enum listing the keys of the HTTP header for the data linked to the user
 * account.
 *
 * @author $Author: FADUBOIS $
 * @version $Revision: 0 $
 */
public interface IHeaderConstante {

    /**
     * Key for the GE identifier.
     */
    String USER_GE_ID = "X-GE-ID";

    /**
     * Key for the GE account.
     */
    String USER_GE_ACCOUNT = "X-GE-ACCOUNT";
}
